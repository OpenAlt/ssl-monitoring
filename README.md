# SSL monitoring

 [![pipeline status](https://gitlab.com/OpenAlt/ssl-monitoring/badges/master/pipeline.svg)](https://gitlab.com/OpenAlt/ssl-monitoring/-/pipelines)

Kontrola platnosti nasazených certifikátů. Seznam domén a portů je uvedený v souboru `domains.txt`.

## Lokální spuštění

Repozitář naklonujte příkazem `git clone --recurse-submodules https://gitlab.com/OpenAlt/ssl-monitoring.git`.

Vyžaduje `make`, `bash` a `openssl`.

```bash
RESULT_FILE="result.txt" make check_expiration
RESULT_FILE="result.txt" make verify_expiration
```

## Lokální spuštění v kontejneru

V kontejneru spusťte `apk add make bash openssl` a pak výše uvedené příkazy.

Samotný kontejner spusťte pomocí:

```bash
podman run \
  -it \
  --workdir "$PWD" \
  -v "$PWD":"$PWD":Z \
  --rm=true \
  docker.io/library/alpine:3.12
```

nebo

```bash
docker run --user "$(id -u):$(id -g)" \
  -it \
  --workdir "$PWD" \
  -v "$PWD":"$PWD" \
  --rm=true \
  docker.io/library/alpine:3.12
```
