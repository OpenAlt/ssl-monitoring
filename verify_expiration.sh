#!/bin/bash
set -u

invalid_certs="$(grep -Ev "\s+(Valid)\s+" "$1")"

if [ -z "$invalid_certs" ]; then
  echo "All certificates are valid."
  exit 0;
else
  echo "There are invalid or expiring certificates:"
  echo "$invalid_certs"
  exit 1;
fi
