CHECK_SCRIPT=./ssl-cert-check/ssl-cert-check
EXPIRATION_INTERVAL_DAYS=21
DOMAINS_LIST=domains.txt

check_expiration:
	bash $(CHECK_SCRIPT) -b -f $(DOMAINS_LIST) -x $(EXPIRATION_INTERVAL_DAYS) | tee ${RESULT_FILE}

verify_expiration:
	bash verify_expiration.sh ${RESULT_FILE}
